jQuery(document).ready(function() {	
	// tabs for home page switching position & clicking function custon
	jQuery(".tabs-nav").insertAfter(".tabs-container");
	jQuery("ul.tabs-nav a").click(function() {
	 var tabid = jQuery(this).attr('href')
	 	jQuery(".tabs-container .tab-content").css( "display", "none");
		jQuery(tabid).fadeIn( "slow", function() {});
	});
	// industry tabs on industry page
	jQuery("#industry-slider_content #tab1").css( "display", "block");
	jQuery("#industry-slider a.aio-icon-box-link").click(function(event) {
	event.preventDefault();
	 var tabid = jQuery(this).attr('href')
	 	jQuery("#industry-slider_content .tab").css( "display", "none");
		jQuery(tabid).fadeIn( "slow", function() {});
	});
	
	jQuery("a.popup_menu").click(function() {
		if(!jQuery('a.popup_menu').hasClass('opened')){
			jQuery('span.popup_menu_text').text('close');
		} else {
			jQuery('span.popup_menu_text').text('Menu');
		}
	});
	
});